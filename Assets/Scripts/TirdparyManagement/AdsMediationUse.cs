﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AdsMediationUse : MonoBehaviour
{
    private UnityAction onRewardAdViewed;
    private string adsZoneId = "0994b9d7b18c7198";

    void Start()
    {
        AppLovin.SetSdkKey("NDD7mdwXcJzFZpS9qkxi5_y5BkaiBcmHVKlIvcM9mybWg6Q34R5BzyLxyuN11qHL4w1lmVu9Z0ymvvp1OzfOWL");
        AppLovin.InitializeSdk();

        AppLovin.SetUnityAdListener("AdsMediationUse");
        AppLovin.LoadRewardedInterstitial();
    }

    public void ShowRewardedAd(UnityAction delayedAction)
    {
        onRewardAdViewed = delayedAction;
#if UNITY_EDITOR
        if (delayedAction != null)
        {
            delayedAction.Invoke();
        }
#elif UNITY_IPHONE
        AppLovin.LoadRewardedInterstitial (adsZoneId);
        AppLovin.ShowRewardedInterstitialForZoneId (adsZoneId);
#elif UNITY_ANDROID
        AppLovin.LoadRewardedInterstitial (adsZoneId);
        AppLovin.ShowRewardedInterstitialForZoneId (adsZoneId);
#endif
    }

    private void OnAdViewedAction()
    {
        if (onRewardAdViewed != null)
        {
            onRewardAdViewed.Invoke();
            onRewardAdViewed = null;
        }
    }

    void onAppLovinEventReceived(string ev)
    {
        if (ev.Contains("REWARDAPPROVEDINFO"))
        {
            OnAdViewedAction();
            Debug.Log("give reward");
        }
        else if (ev.Contains("LOADEDREWARDED"))
        {
            Debug.Log("A rewarded video was successfully loaded.");
        }
        else if (ev.Contains("LOADREWARDEDFAILED"))
        {
            Debug.Log("A rewarded video failed to load.");
        }
        else if (ev.Contains("HIDDENREWARDED"))
        {
            Debug.Log("A rewarded video was closed.  Preload the next rewarded video.");
            AppLovin.LoadRewardedInterstitial(adsZoneId);
        }
    }

}
