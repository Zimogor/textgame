﻿using UnityEngine;
using System.Collections.Generic;


public class ProgressSaver : MonoBehaviour {

	public struct SaveBlob {

		public int nodeId;
		public Dictionary<string, int> values;
	}

	SaveBlob saveBlob;

	void Awake() {
	
		saveBlob = new SaveBlob() {
			values = new Dictionary<string, int>()
		};
	}

	public void SaveProgress() {

		saveBlob.nodeId = Services.Instance.DialogManager.CurNodeId;
		saveBlob.values.Clear();
		Services.Instance.ValuesContainer.CopyValues(saveBlob.values);
	}

	public SaveBlob RestoreProgress() {

		return saveBlob;
	}
}
