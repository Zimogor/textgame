﻿using UnityEngine;
using IPanelListener = GamePanel.IPanelListener;

// порядок скриптов
// Services - для раздачи сервисов

namespace DialogManagement {

	public class DialogManager : MonoBehaviour, IPanelListener {

		[SerializeField] GameObject stepBackButton = null;
		[SerializeField] GameObject menuGO = null;
		[SerializeField] GamePanel gamePanel = null;

		public int CurNodeId { get; private set; }

		ProgressSaver progressSaver;
		ValuesContainer valuesContainer;
		DialogParser dialogParser;
		MessagePanel messagePanel;

		void Awake() {
		
			gamePanel.Listener = this;
			messagePanel = Services.Instance.MessagePanel;
			progressSaver = Services.Instance.ProgressSaver;
			valuesContainer = Services.Instance.ValuesContainer;
			dialogParser = Services.Instance.DialogParser;
		}

		public void RunGame(string jsonString) {

			Node startNode = new Node();
			try {
				startNode = dialogParser.ParseDialogText(jsonString);
			} catch {
				messagePanel.ShowOkMessage("Не удалось распарсить json файл. Воспользуйтесь онлайн валидатором для поиска ошибок");
				return;
			}
			RunGame(startNode);
		}

		void RunGame(Node startNode) {

			valuesContainer.ResetValues();
			menuGO.SetActive(false);
			gamePanel.gameObject.SetActive(true);
			stepBackButton.SetActive(false);
			gamePanel.SetNode(startNode);
		}

		public void BackToMenu() {

			gamePanel.gameObject.SetActive(false);
			menuGO.SetActive(true);
			valuesContainer.ResetValues();
		}

		void IPanelListener.OnValueChanged(Value value) {

			valuesContainer.ChangeValue(value);
		}

		void IPanelListener.OnPreAnswer() {

			progressSaver.SaveProgress();
		}

		void IPanelListener.OnPostAnswer(int nodeId) {

			stepBackButton.SetActive(true);
			CurNodeId = nodeId;
		}

		void IPanelListener.OnRequestInApp(string alias) {

			messagePanel.ShowConfirmMessage($"Будем покупать товар типа:\nalias({alias})?", confirmCallback: () => {
				Services.Instance.Purchaser.RequestPurchase(alias);
			});
		}

		public void OnRewardedAdRequest() {

			Services.Instance.AdsMediationUser.ShowRewardedAd(() => {

				var value = new Value() { alias = "money", value = 500 };
				Services.Instance.ValuesContainer.ChangeValue(value);
			});
		}

		public void OnStepBack() {

			if (!Services.Instance.Purchaser.Subscribed) {

				messagePanel.ShowConfirmMessage("Нужна подписка, чтобы воспользоваться этой функцией. Приобрести подписку?", confirmCallback: () => {
					Services.Instance.Purchaser.RequestSubscription();
				});
				return;
			}

			stepBackButton.SetActive(false);
			var saveBlob = progressSaver.RestoreProgress();
			valuesContainer.RestoreValues(saveBlob.values);
			Node restoreNode = dialogParser.GetNodeById(saveBlob.nodeId);
			gamePanel.SetNode(restoreNode);
			CurNodeId = saveBlob.nodeId;
		}
	}
}