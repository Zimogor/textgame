﻿using UnityEngine;
using UnityEngine.UI;


public class ValueBox : MonoBehaviour {

	public interface IListener {

		void OnClick(string alias);
	}

	[SerializeField] string alias = null;
	[SerializeField] Text valueText = null;

	public IListener Listener { private get; set; }

	ValuesContainer valuesContainer;

	void Awake() {
	
		valuesContainer = Services.Instance.ValuesContainer;
		valuesContainer.OnValueChanged += OnValueChanged;
	}

	void Start() {
	
		valueText.text = valuesContainer.GetValue(alias).ToString();
	}

	void OnValueChanged(string alias, int newValue, int oldValue) {

		if (this.alias == alias)
			valueText.text = newValue.ToString();
	}

	public void OnButtonClick() {

		Listener.OnClick(alias);
	}

	void OnDestroy() {
	
		Listener = null;
	}
}
