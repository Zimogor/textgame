﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


public class MessagePanel : MonoBehaviour {

	[SerializeField] Text text = null;
	[SerializeField] Button button1 = null;
	[SerializeField] Button button2 = null;

	const string OkText = "OK";
	const string CancelText = "Cancel";

	UnityAction button1callback;

	public void ShowOkMessage(string text) {

		this.text.text = text;
		gameObject.SetActive(true);
		button1.gameObject.SetActive(true);
		button1.GetComponentInChildren<Text>().text = OkText;
		button2.gameObject.SetActive(false);
	}

	public void ShowConfirmMessage(string text, UnityAction confirmCallback = null) {

		this.text.text = text;
		gameObject.SetActive(true);
		button1.gameObject.SetActive(true);
		button1.GetComponentInChildren<Text>().text = OkText;
		button1callback = confirmCallback;
		button2.gameObject.SetActive(true);
		button2.GetComponentInChildren<Text>().text = CancelText;
	}

	public void Button1Click() {

		gameObject.SetActive(false);
		button1callback?.Invoke();
		button1callback = null;
	}

	public void Button2Click() {

		gameObject.SetActive(false);
		button1callback = null;
	}

	void OnDestroy() {
	
		button1callback = null;
	}
}
