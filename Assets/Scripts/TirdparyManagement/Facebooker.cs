﻿using UnityEngine;
using Facebook.Unity;
using System.Collections.Generic;


public class Facebooker : MonoBehaviour {

	MessagePanel messagePanel;

	private void Awake() {
		
		messagePanel = Services.Instance.MessagePanel;
	}

	void Start() {
	
		FB.Init(InitCallback);
	}

	void InitCallback() {

		if (!FB.IsInitialized) {

			messagePanel.ShowOkMessage("Не удалось инизиализировать Facebook SDK");
			return;
		}

		FB.ActivateApp();
		var perms = new List<string>(){"public_profile", "email"};
		FB.LogInWithReadPermissions(perms, AuthCallback);
	}

	void AuthCallback (ILoginResult result) {

		if (!FB.IsLoggedIn)
			messagePanel.ShowOkMessage("Не удалось залогировать пользователя в Facebook");
	}
}
