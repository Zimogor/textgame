﻿using UnityEngine;
using UnityEngine.UI;
using DialogManagement;


public class GamePanel : MonoBehaviour, Answer.IListener, ValueBox.IListener {

	public interface IPanelListener {

		void OnValueChanged(Value value);
		void OnPostAnswer(int nodeId);
		void OnPreAnswer();
		void OnRequestInApp(string alias);
	}

	[SerializeField] Text replicaText = null;
	[SerializeField] Answer answerPrefab = null;
	[SerializeField] RectTransform answersTransform = null;
	[SerializeField] ValueBox[] valueBoxes = null;

	public IPanelListener Listener { private get; set; }

	DialogParser dialogParser;

	void Awake() {
	
		foreach (var valueBox in valueBoxes)
			valueBox.Listener = this;
		dialogParser = Services.Instance.DialogParser;
	}

	public void SetNode(Node node) {

		replicaText.text = node.text;

		foreach (RectTransform child in answersTransform)
			Destroy(child.gameObject);

		if (node.nextNodes != null)
			foreach (int nextId in node.nextNodes) {

				var answerButton = Instantiate(answerPrefab, answersTransform);
				var nextNode = dialogParser.GetNodeById(nextId);
				answerButton.Initialize(nextNode.answer, nextId, this);
			}

		ApplyNodeValues(node);
	}

	void ApplyNodeValues(Node node) {

		if (node.values != null)
			foreach (var value in node.values)
				Listener.OnValueChanged(value);
	}

	void Answer.IListener.OnAnswer(int nodeId) {

		Listener.OnPreAnswer();
		var answerNode = dialogParser.GetNodeById(nodeId);

		if (string.IsNullOrEmpty(answerNode.text) && answerNode.nextNodes?.Length == 1) {

			ApplyNodeValues(answerNode);
			answerNode = dialogParser.GetNodeById(answerNode.nextNodes[0]);
		}

		SetNode(answerNode);
		Listener.OnPostAnswer(answerNode.id);
	}

	void ValueBox.IListener.OnClick(string alias) {

		Listener.OnRequestInApp(alias);
	}

	void OnDestroy() {
	
		Listener = null;
	}
}
