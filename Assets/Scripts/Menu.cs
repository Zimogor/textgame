﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using DialogManagement;


public class Menu : MonoBehaviour {

	[SerializeField] Text textField = null;

	DialogManager dialogManager;

	void Awake() {
	
		dialogManager = Services.Instance.DialogManager;
	}

	public void OnSituationClick(string assetName) {

		var textAsset = Resources.Load<TextAsset>(assetName);
		dialogManager.RunGame(textAsset.text);
	}

	public void OnLoadSituationClick() {

		StartCoroutine(LoadSituationCoroutine());
	}

	public void OnBuySubscriptionClick() {

		Services.Instance.MessagePanel.ShowConfirmMessage("Будем покупать подписку?", confirmCallback: () => {
			Services.Instance.Purchaser.RequestSubscription();
		});
	}

	IEnumerator LoadSituationCoroutine() {

		EnableAllButtons(false);
		UnityWebRequest request = UnityWebRequest.Get(textField.text);
		yield return request.SendWebRequest();
		EnableAllButtons(true);
		if (request.isNetworkError || request.isHttpError) {

			Services.Instance.MessagePanel.ShowOkMessage(request.error);
			yield break;
		}

		var jsonString = request.downloadHandler.text;
		dialogManager.RunGame(jsonString);
	}

	void EnableAllButtons(bool enable) {

		foreach(var button in GetComponentsInChildren<Button>())
			button.interactable = enable;
	}
}
