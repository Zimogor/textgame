﻿using UnityEngine;
using UnityEngine.Events;
using System;
using UnityEngine.Purchasing;


namespace Purchasing {

	public class Purchaser : MonoBehaviour, IStoreListener {

		public UnityAction<string, string> OnConsumablePurchased; // alias, product id
		public UnityAction<string> OnSubscriptionPurchased; // product id

		IStoreController controller = null;
		MessagePanel messagePanel;

		const string shopIsNotInited = "Магазин не был инициализирован. Покупка не возможна.";

		const string moneyPackPurchaseId = "com.gooses.interactivestory.moneypack";
		const string fansPackPurchaseId = "com.gooses.interactivestory.fanspack";
		const string subscriptionPurchaseId = "com.gooses.interactivestory.subscription2";

		public bool Inited => controller != null;
		public bool Subscribed { get; private set; } = false;

		void Awake() {
		
			messagePanel = Services.Instance.MessagePanel;
		}

		void Start() {
		
			var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
			builder.AddProduct(moneyPackPurchaseId, ProductType.Consumable);
			builder.AddProduct(fansPackPurchaseId, ProductType.Consumable);
			builder.AddProduct(subscriptionPurchaseId, ProductType.Subscription);
			UnityPurchasing.Initialize(this, builder);
		}

		void IStoreListener.OnInitialized(IStoreController controller, IExtensionProvider extensions) {

			this.controller = controller;
			var subscribeProduct = controller.products.WithID(subscriptionPurchaseId);
			if (subscribeProduct.hasReceipt) {
				
				var info = new SubscriptionManager(subscribeProduct, null).getSubscriptionInfo();
				Subscribed = info.isExpired() == Result.False;
			}
		}

		PurchaseProcessingResult IStoreListener.ProcessPurchase(PurchaseEventArgs e) {

			string productID = e.purchasedProduct.definition.id;
			switch (productID) {

				case moneyPackPurchaseId:
					OnConsumablePurchased("money", productID);
					return PurchaseProcessingResult.Complete;
				case fansPackPurchaseId:
					OnConsumablePurchased("fans", productID);
					return PurchaseProcessingResult.Complete;
				case subscriptionPurchaseId:
					Subscribed = true;
					OnSubscriptionPurchased?.Invoke(productID);
					return PurchaseProcessingResult.Complete;
			}
			throw new NotImplementedException();
		}

		void IStoreListener.OnInitializeFailed(InitializationFailureReason error) {
			Services.Instance.MessagePanel.ShowOkMessage(error.ToString());
		}
		void IStoreListener.OnPurchaseFailed(Product i, PurchaseFailureReason p) {
			Services.Instance.MessagePanel.ShowOkMessage(p.ToString());
		}

		public void RequestSubscription() {

			if (!Inited) {

				messagePanel.ShowOkMessage(shopIsNotInited);
				return;
			}

			if (Subscribed) {

				messagePanel.ShowOkMessage("Подписка уже есть");
				return;
			}

			controller.InitiatePurchase(subscriptionPurchaseId);
		}

		public void RequestPurchase(string alias) {

			if (!Inited) {

				messagePanel.ShowOkMessage(shopIsNotInited);
				return;
			}

			switch (alias) {

				case "money":
					controller.InitiatePurchase(moneyPackPurchaseId);
					return;
				case "fans":
					controller.InitiatePurchase(fansPackPurchaseId);
					return;
			}

			throw new NotImplementedException(alias);
		}

		void OnDestroy() {
		
			OnConsumablePurchased = null;
		}
	}
}