﻿using UnityEngine;
using System;
using UnityEngine.Events;
using System.Collections.Generic;
using DialogManagement;


public class ValuesContainer : MonoBehaviour {

	public UnityAction<string, int, int> OnValueChanged; // alias, newValue, oldValue

	Dictionary<string, int> valuesData = new Dictionary<string, int>();

	void Awake() {
	
		Services.Instance.Purchaser.OnConsumablePurchased += OnConsumablePurchased;
	}

	public void ChangeValue(Value value) {

		if (!valuesData.ContainsKey(value.alias))
			valuesData.Add(value.alias, 0);
		int oldValue = valuesData[value.alias];
		int newValue = oldValue + value.value;
		valuesData[value.alias] = newValue;
		OnValueChanged?.Invoke(value.alias, newValue, oldValue);
	}

	public void ResetValues() {

		var keys = new List<string>(valuesData.Keys);
		foreach (var alias in keys) {

			int oldValue = valuesData[alias];
			valuesData.Remove(alias);
			OnValueChanged?.Invoke(alias, 0, oldValue);
		}
	}

	public void RestoreValues(Dictionary<string, int> restoreDict) {

		var keys = new List<string>(valuesData.Keys);
		foreach (var alias in keys) {

			int oldValue = valuesData[alias];
			int newValue = 0;
			if (restoreDict.ContainsKey(alias))
				newValue = restoreDict[alias];
			if (oldValue == newValue) continue;
			valuesData[alias] = newValue;
			OnValueChanged?.Invoke(alias, newValue, oldValue);
		}
		foreach (var keyvalue in restoreDict) {
			string alias = keyvalue.Key;
			if (valuesData.ContainsKey(alias))
				continue;
			int oldValue = 0;
			int newValue = keyvalue.Value;
			if (oldValue == newValue)
				continue;
			valuesData[alias] = newValue;
			OnValueChanged?.Invoke(alias, newValue, oldValue);
		}
	}

	public void CopyValues(Dictionary<string, int> copyDict) {

		foreach (var keyvalue in valuesData)
			copyDict[keyvalue.Key] = keyvalue.Value;
	}

	public int GetValue(string alias) {

		valuesData.TryGetValue(alias, out int value);
		return value;
	}

	void OnConsumablePurchased(string alias, string productId) {

		switch (alias) {

			case "money":
				ChangeValue(new Value() { alias = alias, value = 10000 });
				break;
			case "fans":
				ChangeValue(new Value() { alias = alias, value = 10000 });
				break;
			default:
				throw new NotImplementedException(alias);
		}
	}

	void OnDestroy() {
	
		OnValueChanged = null;
	}
}
