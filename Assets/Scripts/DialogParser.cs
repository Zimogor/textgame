﻿using UnityEngine;
using System;
using System.Collections.Generic;


namespace DialogManagement {

	[Serializable]
	public struct Value {

		public string alias;
		public int value;
	}

	[Serializable]
	public struct Node {

		public int id;
		public string text;
		public string answer;
		public int[] nextNodes;
		public Value[] values;
	}

	public class DialogParser : MonoBehaviour {

		#pragma warning disable 0649
		[Serializable]
		struct Dialog {

			public int startNodeID;
			public Node[] nodes;
		}
		#pragma warning restore 0649

		Dictionary<int, Node> nodesCache;

		public Node ParseDialogText(string jsonString) {

			nodesCache = new Dictionary<int, Node>();
			var dialog = JsonUtility.FromJson<Dialog>(jsonString);
			foreach (var node in dialog.nodes)
				nodesCache.Add(node.id, node);
			return nodesCache[dialog.startNodeID];
		}

		public Node GetNodeById(int id) {

			return nodesCache[id];
		}
	}

}