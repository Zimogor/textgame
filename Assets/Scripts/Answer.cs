﻿using UnityEngine;
using UnityEngine.UI;


public class Answer : MonoBehaviour {

	public interface IListener {

		void OnAnswer(int dialogId);
	}

	public int Id { get; private set; }

	[SerializeField] Text text = null;

	IListener listener;

	public void Initialize(string text, int id, IListener listener) {

		Id = id;
		this.text.text = text;
		this.listener = listener;
	}

	public void OnClick() {

		listener.OnAnswer(Id);
	}

	void OnDestroy() {
	
		listener = null;
	}
}
