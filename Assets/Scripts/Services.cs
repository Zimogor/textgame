﻿using UnityEngine;
using UnityEngine.Assertions;
using DialogManagement;
using Purchasing;


public class Services : MonoBehaviour {

	[SerializeField] DialogManager dialogManager = null;
	[SerializeField] ValuesContainer valuesContainer = null;
	[SerializeField] DialogParser dialogParser = null;
	[SerializeField] ProgressSaver progressSaver = null;
	[SerializeField] MessagePanel messagePanel = null;
	[SerializeField] Purchaser purchaser = null;
	[SerializeField] AdsMediationUse adsMediationUser = null;

	public static Services Instance { get; private set; }

	void Awake() {
		Assert.IsNull(Instance);

		Instance = this;
	}

	void OnDestroy() {
	
		Instance = null;
	}

	public DialogParser DialogParser => dialogParser;
	public ValuesContainer ValuesContainer => valuesContainer;
	public DialogManager DialogManager => dialogManager;
	public ProgressSaver ProgressSaver => progressSaver;
	public MessagePanel MessagePanel => messagePanel;
	public Purchaser Purchaser => purchaser;
	public AdsMediationUse AdsMediationUser => adsMediationUser;
}
