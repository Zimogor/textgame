﻿using UnityEngine;


public class Metricator : MonoBehaviour {

	void Awake() {
	
		Services.Instance.Purchaser.OnConsumablePurchased += ConsumablePurchased;
		Services.Instance.Purchaser.OnSubscriptionPurchased += OnSubscriptionPurhased;
	}

	void ConsumablePurchased(string alias, string productId) {

		YandexAppMetricaRevenue revenue = new YandexAppMetricaRevenue {
			ProductID = productId
		};
		AppMetrica.Instance.ReportRevenue(revenue);
	}

	void OnSubscriptionPurhased(string productId) {

		YandexAppMetricaRevenue revenue = new YandexAppMetricaRevenue {
			ProductID = productId
		};
		AppMetrica.Instance.ReportRevenue(revenue);
	}
}
